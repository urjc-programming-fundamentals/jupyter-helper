import sys
from io import StringIO
from IPython import get_ipython
from contextlib import contextmanager

@contextmanager
def capture_and_check_output(expected_output):
    """
    Capture and verify the output
    """
    # Reroute the standard output to the StringIO object
    captured_output = StringIO()
    old_stdout = sys.stdout
    sys.stdout = captured_output

    try:
        # Execute the code of the block
        yield

        # Capture the output
        captured_output.seek(0)
        output = captured_output.read()
        sys.stdout = old_stdout

        # Verify if the print is the expected output
        if expected_output in output:
            print("✅: ", output)
        else:
            print("❌: ", output)

    finally:
        # Restore the standard output
        sys.stdout = old_stdout


def check_output_magic(line, cell):
    """
    Magic function to be used as a macro
    """
    expected_output = line.strip()
    ip = get_ipython()
    with capture_and_check_output(expected_output):
        ip.run_cell(cell)

# Register the magic macro
get_ipython().register_magic_function(check_output_magic, magic_kind='cell', magic_name='check_output')

# Now you can use the %check_output
