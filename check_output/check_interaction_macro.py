import sys
import json
from contextlib import contextmanager
from pprint import pprint
from unittest.mock import patch
import urllib.parse
import urllib.request
from IPython import get_ipython


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


def register_interaction(interaction_name):
    quoted_name = urllib.parse.quote(interaction_name)
    url = (f"https://durjc.techer.es/items/notebook/?filter[name][_eq]={quoted_name}"
           "&fields=tasks.name,tasks.description,tasks.tests.input,tasks.tests.output,raw_tests.name,raw_tests.test_code")
    
    # print(url)

    try:
        with urllib.request.urlopen(url) as response:
            response_json = response.read()
            data = json.loads(response_json)["data"][0]
            # print(data)

            dict_by_task = {
                task["name"]: {
                    "name": task["name"],
                    "description": task["description"],
                    "tests": [
                        {"input": test["input"], "output": test["output"]}
                        for test in task["tests"]
                    ],
                }
                for task in data["tasks"]
            }

            raw_code_by_name = {
                raw_test["name"]: raw_test["test_code"]
                for raw_test in data["raw_tests"]
            }
            # print(dict_by_task)
            print("Tasks data loaded.")
    except:
        print(f'Project: {interaction_name} DOESN\'T EXIST', file=sys.stderr)
        return


    @contextmanager
    def intercept_variables(task, ip):
        interactions = task
        
        globals_overwrite = {}
        if "overwrite" in interactions:
            globals_overwrite.update(interactions["overwrite"])
            for key, value in interactions["overwrite"].items():
                if key in ip.user_ns:
                    globals_overwrite[key] = ip.user_ns[key]
                ip.user_ns[key] = value
                
        yield
                    
        ip.user_ns.update(globals_overwrite)


    @contextmanager
    def intercept_task_io(task):
        fails = False
        interactions = task
        
        sys.stdout.write(
            f"{bcolors.HEADER}# {interactions['name']} - {interactions['description']}{bcolors.ENDC}\n"
        )

        if "input_list" in interactions:
            inputs_list = interactions["input_list"]
        else:
            inputs_list = [test["input"] for test in interactions["tests"] if test["input"]]
        
        if "output_list" in interactions:
            output_list = interactions["output_list"]
        else:
            output_list = [
                test["output"] for test in interactions["tests"] if test["output"]
            ]

        def mock_input(*args, **kwargs):
            query = " ".join(args)

            if len(inputs_list) > 0:
                input_string = inputs_list.pop(0)
                sys.stdout.write(
                    f"👨🏻‍💻: {bcolors.OKCYAN}{query}{bcolors.ENDC} <- {bcolors.OKBLUE}{input_string}{bcolors.ENDC}\n"
                )
                sys.stdout.flush()
            else:
                fails = True
                sys.stdout.write(
                    f"👨🏻‍💻: {bcolors.OKCYAN}{query}{bcolors.ENDC} <- {bcolors.WARNING}Unexpected input{bcolors.ENDC}\n"
                )
                sys.stdout.flush()
                input_string = None

            return input_string

        def mock_output(*args, **kwargs):
            args_str = [str(a) for a in args]
            output_strings = " ".join(args_str).replace(" \n", "\n").replace("\n ", "\n")
            
            if len(output_list) > 0:
                validate = output_list.pop(0)
                
                if validate == output_strings:
                    sys.stdout.write(
                        f"✅: {bcolors.OKGREEN}{output_strings}{bcolors.ENDC}\n"
                    )
                    sys.stdout.flush()
                else:
                    fails = True
                    sys.stdout.write(
                        f"❌: {bcolors.WARNING}{output_strings}{bcolors.ENDC}\n"
                        f"{bcolors.WARNING}expected: {bcolors.BOLD}{validate}{bcolors.ENDC}\n"
                    )
                    sys.stdout.flush()
            else:
                sys.stdout.write(
                    f"⁉️: {bcolors.WARNING}Unexpected print: {output_strings}{bcolors.ENDC}\n"
                )
                sys.stdout.flush()

        with patch("builtins.input", side_effect=mock_input), patch(
            "builtins.print", side_effect=mock_output
        ):
            # Execute the code of the block
            yield

        if len(inputs_list) > 0 or len(output_list) > 0:
            sys.stdout.write(
                f"\n {bcolors.WARNING}There are some tests missing{bcolors.ENDC}\n"
            )
            if len(inputs_list) > 0:
                sys.stdout.write(
                    f"\t - {bcolors.WARNING}{len(inputs_list)} inputs{bcolors.ENDC}\n"
                )
            if len(output_list) > 0:
                sys.stdout.write(
                    f"\t - {bcolors.WARNING}{len(output_list)} outputs{bcolors.ENDC}\n"
                )
            # sys.stdout.write("\n -- Details --\n")
            # sys.stdout.flush()
            # pprint(interactions)
            fails = True
            
        if fails and "tip_on_fail" in task and task["tip_on_fail"]:
            sys.stdout.write(f"⚠️: {bcolors.OKCYAN}{task['tip_on_fail']}{bcolors.ENDC}\n")
        
            
    def fetch_tests(test_id: str):
        
        if test_id in dict_by_task:
            return [dict_by_task[test_id]]
        elif test_id in raw_code_by_name:
            sys.stdout.write(f"{raw_code_by_name[test_id]['description']}\n")
            raw_code = raw_code_by_name[test_id]
            return [
                { 
                    "name": f'{test_id}.{idx+1}',
                    "description": raw_test["description"],
                    "overwrite": raw_test["overwrite"] if "overwrite" in raw_test else {},
                    "input_list": [ i for i in raw_test["input"]],
                    "output_list": [ i for i in raw_test["output"]],
                    "tip_on_fail": raw_test["tip_on_fail"]
                }
                for idx, raw_test in enumerate(raw_code['tests'])
            ]

    def intercept_task_io_magic(line, cell):
        """
        Magic function to be used as a macro
        """
        tests_line = line.strip()
        tests = tests_line.split('|')
        for test_id in tests:
            ip = get_ipython()
            expanded_tests = fetch_tests(test_id)
            
            for individual_test in expanded_tests:
                with intercept_variables(individual_test, ip), \
                      intercept_task_io(individual_test):
                    ip.run_cell(cell)

    # Register the magic macro
    get_ipython().register_magic_function(
        intercept_task_io_magic, magic_kind="cell", magic_name="task_io"
    )
