from setuptools import setup, find_packages

setup(
    name="jupyter_helper",
    version="0.10.4",
    packages=find_packages(),
    install_requires=[],
)
